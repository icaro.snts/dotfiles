################################################################
#
# Link i3's config file
#
################################################################

################################################################
# Creating symbolic link to i3 configuration folder
################################################################
if [ -d ~/.config/i3 ]; then
  rm -rf ~/.config/i3
fi
# Só cria o link se ele já não existir
if [ ! -L ~/.config/i3 ];  then
  ln -s ~/dotfiles/config/i3 ~/.config/i3
fi

################################################################
# Creating symbolic link to fonts folder for custom fonts
################################################################
if [ ! -d ~/.fonts ]; then
  mkdir -p ~/.fonts
fi
if [ ! -L ~/.fonts/custom ]; then
  ln -s ~/dotfiles/fonts ~/.fonts/custom
fi

# Criando o arquivo de ssh caso ele ainda não exista
if [ ! -f ~/.ssh/config ]; then
  # se a pasta ainda não existir, a criamos
  if [ ! -d ~/.ssh ]; then
    mkdir ~/.ssh
  fi
fi

################################################################
# Criando o link simbólico para o arquivo de configuração do ZSH
################################################################
#   Se o arquivo existir, o apagamos
if [ -f ~/.zshrc ]; then
  rm ~/.zshrc
fi
#   e criamos o link simbólico
if [ ! -L ~/.zshrc ]; then
  ln -s ~/dotfiles/.zshrc ~/.zshrc
fi

################################################################
# Criando o link simbólico para o arquivo de configuração do ZIM
################################################################
if [ -f ~/.zimrc ]; then
  rm ~/.zimrc
fi
#   e criamos o link simbólico
if [ ! -L ~/.zimrc ]; then
  ln -s ~/dotfiles/.zimrc ~/.zimrc
fi

################################################################
# Creating symbolic link to aliases file
################################################################
if [ -f ~/.aliases ]; then
  rm ~/.aliases
fi
#   e criamos o link simbólico
if [ ! -L ~/.aliases ]; then
  ln -s ~/dotfiles/.aliases ~/.aliases
fi

################################################################
# Creating symbolic link to custom PATH file
################################################################
if [ -f ~/.paths ]; then
  rm ~/.paths
fi

if [ ! -L ~/.paths ]; then
  ln -s ~/dotfiles/.paths ~/.paths
fi

################################################################
# Creating symbolic link to compton configuration file
################################################################
if [ -f ~/.config/compton.conf ]; then
  rm ~/.config/compton.conf
fi

if [ ! -L ~/.config/compton.conf ]; then
  ln -s ~/dotfiles/compton.conf ~/.config/compton.conf
fi

################################################################
# Creating symbolic link to Xresources configuration file
################################################################
if [ -f ~/.Xresources ]; then
  rm ~/.Xresources
fi

if [ ! -L ~/.Xresources ]; then
  ln -s ~/dotfiles/.Xresources ~/.Xresources
fi

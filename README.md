# Geral

Sempre precisando de alguns pacotes, por isso vamos instalá-los de uma vez.

```
$ sudo aptitude install aptitude git vim
```

# i3

Apenas usar o seguinte comando:

```
sudo aptitude install i3
```

# Instalando o i3blocks

Também pode ser instalado pelo apt.

```
sudo aptitude install i3blocks
```

# Melhorando o visual

Pacotes do próprio Ubuntu

```
sudo aptitude install rofi compton lxappearance feh

```

Temas GTK que podem ser instalados

[Paper theme](https://snwh.org/paper/download).

```
sudo add-apt-repository ppa:snwh/pulp
sudo aptitude update
sudo aptitude install paper-gtk-theme paper-icon-theme paper-cursor-theme
```

[Polar Night](https://github.com/baurigae/polar-night).

```
sudo add-apt-repository ppa:noobslab/themes
sudo aptitude update
sudo aptitude install polar-night-gtk
```

Numix

```
sudo add-apt-repository ppa:numix/ppa
sudo aptitude update
sudo aptitude install numix-icon-theme numix-icon-theme-bevel 
```

Feito isso é só executar o `lxappearance` e escolher o `Paper Theme` para o tema e `Roboto` para a fonte.

Para uma melhor exibição das fontes instalamos o [Infinality](http://www.webupd8.org/2013/06/better-font-rendering-in-linux-with.html).
```
sudo add-apt-repository ppa:no1wantdthisname/ppa
sudo aptitude update
sudo aptitude upgrade
sudo aptitude install fontconfig-infinality
```

# Pacotes adicionais

Alguns pacotes precisa ser instalados para melhor funcionamento do sistema.

```
sudo aptitude install xbacklight acpi gnome-screenshot sysstat
```

Java
```
sudo aptitude install python-software-properties
sudo add-apt-repository ppa:webupd8team/java
sudo aptitude update
sudo aptitude oracle-java8-set-installer oracle-java8-set-default
```

# Pacotes de dev

```
$ sudo aptitude install sshfs virtualbox

# zsh
$ aptitude install zsh
$ chsh -s $(which zsh)
$ sh -c "$(wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"

```

# Third-party


HipChat
```
sudo sh -c 'echo "deb https://atlassian.artifactoryonline.com/atlassian/hipchat-apt-client $(lsb_release -c -s) main" > /etc/apt/sources.list.d/atlassian-hipchat4.list'
wget -O - https://atlassian.artifactoryonline.com/atlassian/api/gpg/key/public | sudo apt-key add -
sudo aptitude update
sudo aptitude install hipchat4
```


spotify (https://www.spotify.com/br/download/linux/)
```
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys BBEBDCB318AD50EC6865090613B00F1FD2C19886
echo deb http://repository.spotify.com stable non-free | sudo tee /etc/apt/sources.list.d/spotify.list
sudo aptitude update
sudo aptitude install spotify-client
```

# Deb

1. google-chrome
1. playerctl